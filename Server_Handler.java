//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06
import java.util.*;
import java.io.*;
import socketio.*;

public class Server_Handler implements Runnable {
  
  //Var
  boolean alive = true;
  int index;
  Lotto_Server server;
  String username, password;
  Socket client;
  ArrayList<int[]> zahlen = new ArrayList<int[]>();
  double konto = 0.0;
  double oldkonto = 0.0;
  
  public Server_Handler(Lotto_Server temp, Socket client, int index) {
    this.server = temp;
    this.client = client;
    this.index = index;
  }
  
  public void send(String s) {
    try {
      client.write(s + "\n");
    } catch(Exception e) {
      System.out.println("ErrorHandler: " + e.getMessage());
    }
  }
  
  public void login() {
    try {
      while (client.dataAvailable() < 0);
      username = client.readLine();
      while (client.dataAvailable() < 0);
      password = client.readLine();
      
      //Benutzerabfrage
      FileReader fr = new FileReader("Lotto_Server_Users.txt");
      String inhalt = "";
      boolean weiter = false;
      while (fr.ready()) inhalt = inhalt + (char) fr.read();
      String users[] = inhalt.split("_");
      for (String s : users) {
        if (s.split(":")[0].equals(username) && s.split(":")[1].equals(password)) {
          //Nutzer existiert
          System.out.println("Benutzer '" + username + "' hat sich verbunden");
          konto = Double.parseDouble(s.split(":")[2]);
          oldkonto = konto;
          weiter = true;
          break;
        }
      } // end of for
      if (weiter) {
        client.write("erfolgreich\n");
      } else {
        client.write("fehler\n");
        System.out.println("Fehlgeschlagener Anmeldeversuch");
        alive = false;
        server.handler.remove(index);
        return;
      } // end of if-else
      
      Thread.sleep(1000);
      client.write("/setmoney xx " + konto + "\n");
    } catch(Exception e) {
      System.out.println("Error-Handler: " + e.getMessage());
    }
  }
  
  public void scannen() {
    try {
      while (client.dataAvailable() < 0);
      String inhalt = client.readLine();
      int z [] = new int[6];
      for (int i = 0;i< z.length ;i++ ) {
        z[i] = Integer.parseInt(inhalt.split(":")[(i+1)]);
      } // end of for
      System.out.println("'" + username + "' hat einen Tipp abgesendet: " + z[0] + ", " + z[1] + ", " + z[2] + ", " + z[3] + ", " + z[4] + ", " + z[5]);
      zahlen.add(z);
      
      //Geld
      konto -= 1.50;
      client.write("/setmoney xx " + konto + "\n");
    } catch(Exception e) {
      System.out.println("Error-Handler. " + e.getMessage());
    }
  }
  
  public int addiereGewinn(int anzahlRichtige) {
    switch (anzahlRichtige) {
      case  2: 
        konto += 3;
        return 3;
      case  3: 
        konto += 10;
        return 10;
      case  4: 
        konto += 500;
        return 500;
      case  5: 
        konto += 10000;
        return 10000;
      case  6: 
        konto += 1000000;
        return 1000000;
      default: 
        
    } // end of switch
    return 0;
  }
  
  public void checkeZahlen(int[] richtigezahlen) {
    try {
      client.write("Ziehung\n");
      
      System.out.println("Zahlen von '" + username + "' werden �berpr�ft");
      int anzahl = 0;
      int scheinnummer = 0;
      for (int[] z : zahlen) {
        //einzelne Tipps durchgehen
        for (int i = 0;i < z.length ;i++ ) {
          for (int a = 0; a < richtigezahlen.length ;a++ ) {
            if (z[i] == richtigezahlen[a]) anzahl++;
          } // end of for
        } // end of for
        System.out.println("'" + username + "' hat in Schein " + (scheinnummer+1) + " insgesamt " + anzahl + " richtige Zahlen");
 
        //Senden (zahl1, zahl2, zahl3, zahl4, zahl5, zahl6, anzahlRichtigerZahlen, Scheinnummer, Gewinn)
        client.write(richtigezahlen[0] + ";" + richtigezahlen[1] + ";" + richtigezahlen[2] + ";" + richtigezahlen[3] + ";" + richtigezahlen[4] + ";" + richtigezahlen[5] + ";" + anzahl + ";" + scheinnummer + ";" + addiereGewinn(anzahl) + "\n");
        scheinnummer ++; 
        anzahl = 0; 
      }  
      client.write("Ende\n");
      Thread.sleep(100);
      
      client.write(konto + "\n");
      Thread.sleep(1000);
      
      
      //Konto aktualisieren
      FileReader fr = new FileReader("Lotto_Server_Users.txt");
      String inhalt = "";
      while (fr.ready()) inhalt = inhalt + (char) fr.read();
      String links =  username + ":" + password + ":" + oldkonto;
      String rechts = username + ":" + password + ":" + konto;
      inhalt = inhalt.replace(links, rechts);
      System.out.println(links + " " + rechts);
      fr.close();
      FileWriter fw = new FileWriter("Lotto_Server_Users.txt");
      fw.write(inhalt);
      fw.close();
    } catch(Exception e) {
      System.out.println("Error-Handler: " + e.getMessage());
    }
    System.out.println("");
  }
  
  public void run() {
    login();
    while (alive) { 
      try {
        scannen();
      } catch(Exception e) {
        System.out.println("Error-Handler: " + e.getMessage());
      }
    } // end of while
    System.out.println("Thread wurde beendet");
  }

} // end of class Server_Handler

